/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 * @author Rafi Rizha Syakhari
 * Class Main dari Class Dosen, Orang dan Pegawai
 */
public class Modul4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Orang lutfi = new Pegawai("Lutfi", new Date(1997, 8, 31), "7828", "STIS", "IT"); //Inisiasi variabel dan Instansiasi Objek
        lutfi.setAlamat("Otista 64 C");
        
        System.out.println(lutfi);
        System.out.println(lutfi.getAlamat());
    }
    
}
