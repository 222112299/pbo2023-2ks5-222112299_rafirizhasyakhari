/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modul4;

/**
 * @author Rafi Rizha Syakhari
 * Class interface yang memberikan beberapa methods yang semuanya harus diimplementasikan pada class yang menggunakannya
 */
public interface Dosen {
    public String getNIDN();
    public void setNIDN(String NIDN);
    public String getKelompokKeahlian();
    public void setKelompokKeahlian(String kelompokKeahlian);
}
