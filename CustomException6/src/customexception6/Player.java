/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package customexception6;

/**
 *
 * @author ASUS
 */
public class Player {
    private String nickname;
    private int poinrank;
    
    public Player(String nickname, int poinrank){
        this.nickname = nickname;
        this.poinrank = poinrank;
}

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPoinrank(int poinrank) {
        this.poinrank = poinrank;
    }
    
    public String getNickname() {
        return nickname;
    }

    public int getPoinrank() {
        return poinrank;
    }
    
    
}
