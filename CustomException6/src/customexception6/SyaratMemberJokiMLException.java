/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package customexception6;

/**
 *
 * @author ASUS
 */
public class SyaratMemberJokiMLException extends Exception {
    public SyaratMemberJokiMLException(){
        super("Gagal masuk joki");
    }
    public SyaratMemberJokiMLException(String errorMessage){
        super("Gagal masuk joki karena " + errorMessage);
    }
    
}

