/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package customexception6;

/**
 *
 * @author ASUS
 */
public class SistemMasuk {
    public SistemMasuk(Player player) throws SyaratMemberJokiMLException{
        if(player.getPoinrank()< 1000){
            throw new SyaratMemberJokiMLException("Kurang Poin");
        }
        System.out.println(String.format("Selamat kamu (%s) masuk ke dalam member Joki Mobile Legends ! ", player.getNickname()));
    }
    
}
