/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package customexception6;

/**
 *
 * @author ASUS
 */
public class CustomExceptionTest {
    public static void main(String[] args) {
        Player p1 = new Player("Pacaa", 2525);
        try {    
            SistemMasuk sm = new SistemMasuk(p1);
        } catch (SyaratMemberJokiMLException ex) {
            System.out.println(ex);
        } finally{
            System.out.println("Selesai");
        }
    }
}
