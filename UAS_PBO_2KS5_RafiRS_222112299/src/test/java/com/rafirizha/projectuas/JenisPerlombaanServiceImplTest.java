/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas;

import com.rafirizha.projectuas.dto.JenisPerlombaanDto;
import com.rafirizha.projectuas.entity.JenisPerlombaan;
import com.rafirizha.projectuas.mapper.JenisPerlombaanMapper;
import com.rafirizha.projectuas.repository.JenisPerlombaanRepository;
import com.rafirizha.projectuas.service.impl.JenisPerlombaanServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class JenisPerlombaanServiceImplTest {

    @Mock
    private JenisPerlombaanRepository jenisPerlombaanRepository;

    @InjectMocks
    private JenisPerlombaanServiceImpl jenisPerlombaanService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetJenisPerlombaanById() {
        Long id = 1L;
        JenisPerlombaan jenisPerlombaan = new JenisPerlombaan();
        jenisPerlombaan.setId(id);
        jenisPerlombaan.setNama("Contoh Perlombaan");

        when(jenisPerlombaanRepository.findById(id)).thenReturn(Optional.of(jenisPerlombaan));

        JenisPerlombaanDto result = jenisPerlombaanService.getJenisPerlombaanById(id);

        assertEquals(jenisPerlombaan.getId(), result.getId());
        assertEquals(jenisPerlombaan.getNama(), result.getNama());
    }

    @Test
    public void testGetAllJenisPerlombaan() {
        JenisPerlombaan jenisPerlombaan1 = new JenisPerlombaan();
        jenisPerlombaan1.setId(1L);
        jenisPerlombaan1.setNama("Perlombaan 1");

        JenisPerlombaan jenisPerlombaan2 = new JenisPerlombaan();
        jenisPerlombaan2.setId(2L);
        jenisPerlombaan2.setNama("Perlombaan 2");

        List<JenisPerlombaan> jenisPerlombaanList = Arrays.asList(jenisPerlombaan1, jenisPerlombaan2);

        when(jenisPerlombaanRepository.findAll()).thenReturn(jenisPerlombaanList);

        List<JenisPerlombaanDto> result = jenisPerlombaanService.getAllJenisPerlombaan();

        assertEquals(jenisPerlombaanList.size(), result.size());
        assertEquals(jenisPerlombaanList.get(0).getId(), result.get(0).getId());
        assertEquals(jenisPerlombaanList.get(0).getNama(), result.get(0).getNama());
        assertEquals(jenisPerlombaanList.get(1).getId(), result.get(1).getId());
        assertEquals(jenisPerlombaanList.get(1).getNama(), result.get(1).getNama());
    }

    @Test
    public void testCreateJenisPerlombaan() {
        JenisPerlombaanDto jenisPerlombaanDto = new JenisPerlombaanDto();
        jenisPerlombaanDto.setNama("Contoh Perlombaan");

        JenisPerlombaan jenisPerlombaan = new JenisPerlombaan();
        jenisPerlombaan.setNama(jenisPerlombaanDto.getNama());

        when(jenisPerlombaanRepository.save(any(JenisPerlombaan.class))).thenReturn(jenisPerlombaan);

        JenisPerlombaanDto result = jenisPerlombaanService.createJenisPerlombaan(jenisPerlombaanDto);

        assertEquals(jenisPerlombaan.getNama(), result.getNama());
    }

    @Test
    public void testUpdateJenisPerlombaan() {
        Long id = 1L;
        JenisPerlombaanDto jenisPerlombaanDto = new JenisPerlombaanDto();
        jenisPerlombaanDto.setNama("Updated Perlombaan");

        JenisPerlombaan existingJenisPerlombaan = new JenisPerlombaan();
        existingJenisPerlombaan.setId(id);
        existingJenisPerlombaan.setNama("Original Perlombaan");

        JenisPerlombaan updatedJenisPerlombaan = new JenisPerlombaan();
        updatedJenisPerlombaan.setId(id);
        updatedJenisPerlombaan.setNama(jenisPerlombaanDto.getNama());

        when(jenisPerlombaanRepository.findById(id)).thenReturn(Optional.of(existingJenisPerlombaan));
        when(jenisPerlombaanRepository.save(any(JenisPerlombaan.class))).thenReturn(updatedJenisPerlombaan);

        JenisPerlombaanDto result = jenisPerlombaanService.updateJenisPerlombaan(id, jenisPerlombaanDto);

        assertEquals(updatedJenisPerlombaan.getNama(), result.getNama());
    }

    @Test
    public void testDeleteJenisPerlombaan() {
        Long id = 1L;

        jenisPerlombaanService.deleteJenisPerlombaan(id);

        verify(jenisPerlombaanRepository, times(1)).deleteById(id);
    }
}


