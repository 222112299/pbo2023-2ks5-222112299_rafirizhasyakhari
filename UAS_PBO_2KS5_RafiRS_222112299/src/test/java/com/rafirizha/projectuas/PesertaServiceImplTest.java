/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas;

import com.rafirizha.projectuas.dto.JenisPerlombaanDto;
import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.dto.UserDto;
import com.rafirizha.projectuas.entity.JenisPerlombaan;
import com.rafirizha.projectuas.entity.Peserta;
import com.rafirizha.projectuas.entity.User;
import com.rafirizha.projectuas.mapper.PesertaMapper;
import com.rafirizha.projectuas.repository.PesertaRepository;
import com.rafirizha.projectuas.repository.UserRepository;
import com.rafirizha.projectuas.service.impl.PesertaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PesertaServiceImplTest {

    @Mock
    private PesertaRepository pesertaRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private PesertaServiceImpl pesertaService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllPeserta() {
        List<Peserta> pesertaList = new ArrayList<>();
        Peserta peserta1 = new Peserta();
        Peserta peserta2 = new Peserta();
        
        JenisPerlombaan jenisPerlombaan1 = new JenisPerlombaan();
        JenisPerlombaan jenisPerlombaan2 = new JenisPerlombaan();
        
        peserta1.setJenisPerlombaan(jenisPerlombaan1);
        peserta2.setJenisPerlombaan(jenisPerlombaan2);

        pesertaList.add(peserta1);
        pesertaList.add(peserta2);

        when(pesertaRepository.findAll()).thenReturn(pesertaList);

        List<PesertaDto> result = pesertaService.getAllPeserta();
        assertEquals(pesertaList.size(), result.size());
    }


    @Test
    public void testGetPesertaById_ExistingId() {
        Long id = 1L;
        Peserta peserta = new Peserta();
        peserta.setId(id);

        // Create mock JenisPerlombaan object
        JenisPerlombaan jenisPerlombaan = new JenisPerlombaan();
        peserta.setJenisPerlombaan(jenisPerlombaan);

        when(pesertaRepository.findById(id)).thenReturn(Optional.of(peserta));

        PesertaDto result = pesertaService.getPesertaById(id);

        assertNotNull(result);
        assertEquals(id, result.getId());
    }


    @Test
    public void testGetPesertaById_NonExistingId() {
        Long id = 1L;

        when(pesertaRepository.findById(id)).thenReturn(Optional.empty());

        PesertaDto result = pesertaService.getPesertaById(id);

        assertNull(result);
    }

    @Test
    public void testCreatePeserta() {
        Long userId = 1L;
        PesertaDto pesertaDto = new PesertaDto();
        pesertaDto.setUserId(userId);

        Peserta existingPeserta = new Peserta();
        existingPeserta.setId(userId);

        when(pesertaRepository.findByUserId(userId)).thenReturn(existingPeserta);

        assertThrows(IllegalStateException.class, () -> pesertaService.createPeserta(pesertaDto));
    }

    @Test
    public void testUpdatePeserta_ExistingId() {
        Long id = 1L;
        PesertaDto pesertaDto = new PesertaDto();
        pesertaDto.setNama("Updated Peserta");

        JenisPerlombaanDto jenisPerlombaanDto = new JenisPerlombaanDto();
        jenisPerlombaanDto.setId(1L);
        jenisPerlombaanDto.setNama("Perlombaan A");

        pesertaDto.setJenisPerlombaan(jenisPerlombaanDto);

        Peserta existingPeserta = new Peserta();
        existingPeserta.setId(id);
        existingPeserta.setNama("Original Peserta");

        Peserta updatedPeserta = new Peserta();
        updatedPeserta.setId(id);
        updatedPeserta.setNama(pesertaDto.getNama());
        updatedPeserta.setJenisPerlombaan(new JenisPerlombaan());

        when(pesertaRepository.findById(id)).thenReturn(Optional.of(existingPeserta));
        when(pesertaRepository.save(any(Peserta.class))).thenReturn(updatedPeserta);

        PesertaDto result = pesertaService.updatePeserta(id, pesertaDto);

        assertNotNull(result);
        assertEquals(pesertaDto.getNama(), result.getNama());
    }


    @Test
    public void testUpdatePeserta_NonExistingId() {
        Long id = 1L;
        PesertaDto pesertaDto = new PesertaDto();
        pesertaDto.setNama("Updated Peserta");

        when(pesertaRepository.findById(id)).thenReturn(Optional.empty());

        PesertaDto result = pesertaService.updatePeserta(id, pesertaDto);

        assertNull(result);
    }

    @Test
    public void testDeletePeserta_ExistingId() {
        Long id = 1L;
        Peserta peserta = new Peserta();
        peserta.setId(id);

        Optional<Peserta> pesertaOptional = Optional.of(peserta);
        when(pesertaRepository.findById(id)).thenReturn(pesertaOptional);

        User user = new User();
        user.setPeserta(peserta);
        peserta.setUser(user);
        when(pesertaRepository.findById(id)).thenReturn(pesertaOptional);
        when(pesertaRepository.save(any(Peserta.class))).thenReturn(peserta);

        pesertaService.deletePeserta(id);

        verify(userRepository, times(1)).save(user);
        verify(pesertaRepository, times(1)).delete(peserta);
    }

    @Test
    public void testDeletePeserta_NonExistingId() {
        Long id = 1L;
        when(pesertaRepository.findById(id)).thenReturn(Optional.empty());

        pesertaService.deletePeserta(id);

        verify(userRepository, never()).save(any(User.class));
        verify(pesertaRepository, never()).delete(any(Peserta.class));
    }
}

