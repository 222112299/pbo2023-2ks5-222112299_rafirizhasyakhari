/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
*
*@author rafi
*/

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;

    @NotEmpty(message = "Nama depan tidak boleh kosong!")
    private String firstName;

    @NotEmpty(message = "Nama belakang tidak boleh kosong!")
    private String lastName;

    @NotEmpty(message = "Email tidak boleh kosong")
    @Email
    private String email;

    @NotEmpty(message = "Password tidak boleh kosong")
    @Pattern(regexp = "^(?=.*[A-Z])(?=.*\\d).{4,}$",
            message = "Password harus terdiri dari minimal 4 karakter, 1 huruf besar, dan 1 angka")
    private String password;

    private String role;
    
}

