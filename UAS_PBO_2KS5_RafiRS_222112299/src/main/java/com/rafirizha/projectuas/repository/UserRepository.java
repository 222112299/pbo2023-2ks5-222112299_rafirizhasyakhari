/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.repository;

import com.rafirizha.projectuas.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
/**
 *
 * @author pinaa
 */

public interface UserRepository extends JpaRepository<User, Long> {
    
    User findByEmail(String email);
}
