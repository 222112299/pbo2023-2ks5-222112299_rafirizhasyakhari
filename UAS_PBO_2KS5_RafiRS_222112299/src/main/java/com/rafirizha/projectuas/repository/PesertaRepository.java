package com.rafirizha.projectuas.repository;

import com.rafirizha.projectuas.entity.Peserta;
import com.rafirizha.projectuas.entity.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PesertaRepository extends JpaRepository<Peserta, Long> {
    // Tambahkan method findById
    Peserta findByIdAndJenisPerlombaanId(Long id, Long jenisPerlombaanId);
    Peserta findByUserId(Long id);
    public boolean existsByUserId(Long userId);
    List<Peserta> findByUser(User user);
    Page<Peserta> findByNamaContainingIgnoreCase(String keyword, Pageable pageable);
    
//    Page<Tim> findByNameContainingIgnoreCase(String keyword, Pageable pageable);
//    Pageable
//    @Query("SELECT s from Student s WHERE " +
//            " s.firstName LIKE CONCAT('%', :query, '%') OR " +
//            " s.lastName LIKE CONCAT('%', :query, '%')")
//    List<Tim> searchStudent(String query);
}
