package com.rafirizha.projectuas.mapper;

import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.entity.Peserta;

public class PesertaMapper {
    public static PesertaDto mapToPesertaDto(Peserta peserta) {
        PesertaDto pesertaDto = new PesertaDto();
        pesertaDto.setId(peserta.getId());
        pesertaDto.setNama(peserta.getNama());
        pesertaDto.setJenisPerlombaan(JenisPerlombaanMapper.mapToJenisPerlombaanDto(peserta.getJenisPerlombaan()));
        pesertaDto.setJenisPerlombaanNama(peserta.getJenisPerlombaanNama());
        return pesertaDto;
    }

    public static Peserta mapToPeserta(PesertaDto pesertaDto) {
        Peserta peserta = new Peserta();
        peserta.setId(pesertaDto.getId());
        peserta.setNama(pesertaDto.getNama());
        peserta.setJenisPerlombaan(JenisPerlombaanMapper.mapToJenisPerlombaan(pesertaDto.getJenisPerlombaan()));
        peserta.setUser(UserMapper.mapToUser(pesertaDto.getUser()));
        return peserta;
    }
}
