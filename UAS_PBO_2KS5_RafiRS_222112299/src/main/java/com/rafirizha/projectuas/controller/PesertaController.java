/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.controller;

import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.dto.JenisPerlombaanDto; 
import com.rafirizha.projectuas.entity.Peserta;
import com.rafirizha.projectuas.mapper.PesertaMapper;
import com.rafirizha.projectuas.service.JenisPerlombaanService; 
import com.rafirizha.projectuas.security.CustomUserDetailsService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;



import java.util.List; 
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import com.rafirizha.projectuas.service.PesertaService;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/peserta")
public class PesertaController {
    private PesertaService pesertaService;
    private JenisPerlombaanService jenisPerlombaanService; // Tambahkan ini
    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    public PesertaController(PesertaService pesertaService
            , JenisPerlombaanService jenisPerlombaanService
            , CustomUserDetailsService userDetailsService){
        this.pesertaService = pesertaService;
        this.jenisPerlombaanService = jenisPerlombaanService;
        this.userDetailsService = userDetailsService;
    }

    // @GetMapping("/form")
    // public String showForm(Model model) {
    //     model.addAttribute("pesertaDto", new PesertaDto());
    //     return "peserta-form";
    // }

    // @PostMapping("/create")
    // public String createPeserta(@ModelAttribute("pesertaDto") PesertaDto pesertaDto) {
    //     pesertaService.createPeserta(pesertaDto);
    //     return "redirect:/peserta/list";
    // }
    
    /*----------------------------------
    * START - Handler untuk list peserta
    ------------------------------------*/ 
    //Admin
    @GetMapping("/admin/list")
    public String showsPesertaList(Model model,
            @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue="1") int page,
            @RequestParam(defaultValue="5") int size){
        List<Peserta> pesertas;
        List<PesertaDto> pesertaDtos = this.pesertaService.getAllPeserta();
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Peserta> pesertaPage;
        
        if (keyword == null) {
            pesertaPage = pesertaService.findAll(pageable);
        } else {
            pesertaPage = pesertaService.findByNamaContainingIgnoreCase(keyword, pageable);
            model.addAttribute("keyword", keyword);
        }
        
        pesertas = pesertaPage.getContent();
        pesertaDtos = pesertas.stream()
                .map(peserta -> PesertaMapper.mapToPesertaDto(peserta))
                .collect(Collectors.toList());
        model.addAttribute("pesertaDtos", pesertaDtos);
        model.addAttribute("currentPage", pesertaPage.getNumber() + 1);
        model.addAttribute("totalItems", pesertaPage.getTotalElements());
        model.addAttribute("totalPages", pesertaPage.getTotalPages());
        model.addAttribute("pageSize", size);
        
        List<Integer> pageSizes = Arrays.asList(5,10,15,20); //Pilihan ukuran
        model.addAttribute("pageSizes", pageSizes);
        return "admin/admin-peserta-list";
    }

    //User - PesertaBy{id}
    @GetMapping("/myinformation")
    public String showPesertaList(Model model, Authentication authentication) {
            String currentPrincipalName = authentication.getName();
            List<PesertaDto> pesertaList = pesertaService.getPesertaByEmail(currentPrincipalName);
            
        model.addAttribute("pesertaList", pesertaList);
        return "my-information";
    }
    //User - All Peserta
    @GetMapping("/pesertalist")
    public String showedPesertaList(Model model,
            @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue="1") int page,
            @RequestParam(defaultValue="5") int size) {
                List<Peserta> pesertas;
        List<PesertaDto> pesertaDtos = this.pesertaService.getAllPeserta();
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Peserta> pesertaPage;
        
        if (keyword == null) {
            pesertaPage = pesertaService.findAll(pageable);
        } else {
            pesertaPage = pesertaService.findByNamaContainingIgnoreCase(keyword, pageable);
            model.addAttribute("keyword", keyword);
        }
        
        pesertas = pesertaPage.getContent();
        pesertaDtos = pesertas.stream()
                .map(peserta -> PesertaMapper.mapToPesertaDto(peserta))
                .collect(Collectors.toList());
        model.addAttribute("pesertaDtos", pesertaDtos);
        model.addAttribute("currentPage", pesertaPage.getNumber() + 1);
        model.addAttribute("totalItems", pesertaPage.getTotalElements());
        model.addAttribute("totalPages", pesertaPage.getTotalPages());
        model.addAttribute("pageSize", size);

        List<Integer> pageSizes = Arrays.asList(5,10,15,20); 
        model.addAttribute("pageSizes", pageSizes);

        return "list-of-peserta";
    }
    /*----------------------------------
    * END - Handler untuk list peserta
    ------------------------------------*/ 
    
    /*----------------------------------
    * START - Handler untuk CRUD peserta
    ------------------------------------*/ 
    //User - Update
    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") Long id, Model model) {
        PesertaDto pesertaDto = pesertaService.getPesertaById(id);
        List<JenisPerlombaanDto> jenisPerlombaanList = jenisPerlombaanService.getAllJenisPerlombaan();
        model.addAttribute("jenisPerlombaanList", jenisPerlombaanList);
        model.addAttribute("pesertaDto", pesertaDto);
        return "update-form";
    }
    @PostMapping("/update/{id}")    
    public String updatePeserta(@PathVariable("id") Long id, @ModelAttribute("pesertaDto") PesertaDto pesertaDto) {
        pesertaService.updatePeserta(id, pesertaDto);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String loggedInUsername = authentication.getName();
    
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        boolean isAdmin = authorities.stream().anyMatch(auth -> auth.getAuthority().equals("ROLE_ADMIN"));
    
        if (isAdmin) {
            return "redirect:/peserta/admin/list";
        } else {
            return "redirect:/peserta/myinformation";
        }
    }
    //User - Delete
    @GetMapping("/delete/{id}")
    public String deletePeserta(@Valid @PathVariable("id") Long id) {
        pesertaService.deletePeserta(id);
        return "redirect:/peserta/myinformation";
    }

    /* ------------------------------------
    *  START - Handler Pendaftaran peserta
    *  ------------------------------------*/ 
    @GetMapping("/daftar-perlombaan")
    public String showDaftarPerlombaanForm(Model model) {
        PesertaDto pesertaDto = new PesertaDto();
        List<JenisPerlombaanDto> jenisPerlombaanList = jenisPerlombaanService.getAllJenisPerlombaan();
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserId = authentication.getName();
        Long userId = userDetailsService.getIdUserByUsername(currentUserId);
        
        pesertaDto.setUserId(userId);
        
        model.addAttribute("currentUserId", userId); // Tambahkan atribut currentUserId
        model.addAttribute("pesertaDto", new PesertaDto());
        model.addAttribute("jenisPerlombaanList", jenisPerlombaanList);

        return "daftar-perlombaan-form";
    }

    @PostMapping("/daftar-perlombaan/process")
    public String processDaftarPerlombaan(
        @ModelAttribute("pesertaDto") PesertaDto pesertaDto, Authentication authentication) {
        String currentPrincipalName = authentication.getName();
        Long userId = userDetailsService.getIdUserByUsername(currentPrincipalName);
        pesertaDto.setUserId(userId);
        pesertaService.createPeserta(pesertaDto);
        return "redirect:/peserta/myinformation";
    }
    /* ------------------------------------
    *  END - Handler Pendaftaran peserta
    *  ------------------------------------*/ 
}
