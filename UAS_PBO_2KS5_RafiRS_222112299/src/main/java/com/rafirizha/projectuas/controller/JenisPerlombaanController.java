package com.rafirizha.projectuas.controller;

import com.rafirizha.projectuas.dto.JenisPerlombaanDto;
import com.rafirizha.projectuas.mapper.JenisPerlombaanMapper;
import com.rafirizha.projectuas.repository.JenisPerlombaanRepository;
import com.rafirizha.projectuas.service.JenisPerlombaanService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class JenisPerlombaanController {
    
    private final JenisPerlombaanService jenisPerlombaanService;

    @Autowired
    public JenisPerlombaanController(JenisPerlombaanService jenisPerlombaanService) {
        this.jenisPerlombaanService = jenisPerlombaanService;
    }
    
    @GetMapping("/jenis-perlombaan")
    public String getAllJenisPerlombaan(Model model) {
        model.addAttribute("jenisPerlombaanList", jenisPerlombaanService.getAllJenisPerlombaan());
        return "jenis-perlombaan-list";
    }
    
    @GetMapping("/jenis-perlombaan/{id}")
    public String getJenisPerlombaanById(@PathVariable("id") Long id, Model model) {
        JenisPerlombaanDto jenisPerlombaanDto = jenisPerlombaanService.getJenisPerlombaanById(id);
        if (jenisPerlombaanDto != null) {
            model.addAttribute("jenisPerlombaan", jenisPerlombaanDto);
            return "jenis-perlombaan-detail";
        }
        return "redirect:/jenis-perlombaan";
    }
    
    
    @GetMapping("/jenis-perlombaan/update/{id}")
    public String showUpdateJenisPerlombaanForm(@PathVariable("id") Long id, Model model) {
        JenisPerlombaanDto jenisPerlombaanDto = jenisPerlombaanService.getJenisPerlombaanById(id);
        if (jenisPerlombaanDto != null) {
            model.addAttribute("jenisPerlombaan", jenisPerlombaanDto);
            return "jenis-perlombaan-form";
        }
        return "redirect:/jenis-perlombaan";
    }
    
    @PostMapping("/jenis-perlombaan/update/{id}")
    public String updateJenisPerlombaan(@PathVariable("id") Long id, JenisPerlombaanDto jenisPerlombaanDto) {
        jenisPerlombaanService.updateJenisPerlombaan(id, jenisPerlombaanDto);
        return "redirect:/jenis-perlombaan";
    }
    
    @GetMapping("/jenis-perlombaan/delete/{id}")
    public String deleteJenisPerlombaan(@PathVariable("id") Long id) {
        jenisPerlombaanService.deleteJenisPerlombaan(id);
        return "redirect:/jenisperlombaan/form";
    }
    
    @GetMapping("/jenisperlombaan/form")
    public String showForm(Model model) {
        List<JenisPerlombaanDto> jenisPerlombaanList = jenisPerlombaanService.getAllJenisPerlombaan();
        model.addAttribute("jenisPerlombaanList", jenisPerlombaanList);
        model.addAttribute("jenisPerlombaanDto", new JenisPerlombaanDto());
        return "admin/add-jenisperlombaan";
    }

    @PostMapping("/jenisperlombaan/save")
    public String saveJenisPerlombaan(@ModelAttribute("jenisPerlombaanDto") JenisPerlombaanDto jenisPerlombaanDto) {
        jenisPerlombaanService.createJenisPerlombaan(jenisPerlombaanDto);
        return "redirect:/jenisperlombaan/form";
    }
}
