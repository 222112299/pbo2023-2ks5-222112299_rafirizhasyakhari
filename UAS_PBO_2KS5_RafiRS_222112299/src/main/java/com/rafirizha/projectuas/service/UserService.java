/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.rafirizha.projectuas.service;

import com.rafirizha.projectuas.dto.UserDto;
import com.rafirizha.projectuas.entity.User;
import java.util.List;


/**
 *
 * @author ASUS
 */
public interface UserService {
    void saveUser(UserDto userDto);
    User findUserByEmail(String email);
    List<UserDto> findAllUsers();
}

