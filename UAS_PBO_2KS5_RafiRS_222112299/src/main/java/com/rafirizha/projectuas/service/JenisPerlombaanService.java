/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.rafirizha.projectuas.service;

import com.rafirizha.projectuas.dto.JenisPerlombaanDto;

import java.util.List;

public interface JenisPerlombaanService {
    JenisPerlombaanDto getJenisPerlombaanById(Long id);
    List<JenisPerlombaanDto> getAllJenisPerlombaan();
    JenisPerlombaanDto createJenisPerlombaan(JenisPerlombaanDto jenisPerlombaanDto);
    JenisPerlombaanDto updateJenisPerlombaan(Long id, JenisPerlombaanDto jenisPerlombaanDto);
    void deleteJenisPerlombaan(Long id);
}
