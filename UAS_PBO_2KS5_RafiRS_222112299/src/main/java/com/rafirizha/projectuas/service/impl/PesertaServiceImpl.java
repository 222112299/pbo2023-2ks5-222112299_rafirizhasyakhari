package com.rafirizha.projectuas.service.impl;

import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.entity.JenisPerlombaan;
import com.rafirizha.projectuas.entity.Peserta;
import com.rafirizha.projectuas.entity.User;
import com.rafirizha.projectuas.mapper.JenisPerlombaanMapper;
import com.rafirizha.projectuas.mapper.PesertaMapper;
import com.rafirizha.projectuas.repository.UserRepository;
import java.util.Collections;
import java.util.HashSet;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import com.rafirizha.projectuas.repository.PesertaRepository;
import com.rafirizha.projectuas.service.PesertaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Service
public class PesertaServiceImpl implements PesertaService {

    private final PesertaRepository pesertaRepository;
    private final UserRepository userRepository;

    @Autowired
    public PesertaServiceImpl(PesertaRepository pesertaRepository, UserRepository userRepository) {
        this.pesertaRepository = pesertaRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<PesertaDto> getAllPeserta() {
        List<Peserta> pesertaList = pesertaRepository.findAll();
        return pesertaList.stream()
                .map(PesertaMapper::mapToPesertaDto)
                .collect(Collectors.toList());
    }

    @Override
    public PesertaDto getPesertaById(Long id) {
        Optional<Peserta> pesertaOptional = pesertaRepository.findById(id);
        return pesertaOptional.map(PesertaMapper::mapToPesertaDto).orElse(null);
    }

    @Override
    public PesertaDto createPeserta(PesertaDto pesertaDto) {
        Long userId = pesertaDto.getUserId();
        Peserta existingPeserta = pesertaRepository.findByUserId(userId);
        
        if (existingPeserta != null) {
        throw new IllegalStateException("Peserta already exists");
    }
        Peserta peserta = PesertaMapper.mapToPeserta(pesertaDto);
        Peserta savedPeserta = pesertaRepository.save(peserta);
        return PesertaMapper.mapToPesertaDto(savedPeserta);
    }

    @Override
    public PesertaDto updatePeserta(Long id, PesertaDto pesertaDto) {
        Optional<Peserta> pesertaOptional = pesertaRepository.findById(id);
        if (pesertaOptional.isPresent()) {
            Peserta peserta = pesertaOptional.get();
            peserta.setNama(pesertaDto.getNama());
            JenisPerlombaan jenisPerlombaan = JenisPerlombaanMapper.mapToJenisPerlombaan(pesertaDto.getJenisPerlombaan());
            peserta.setJenisPerlombaan(jenisPerlombaan);
            

            // Lakukan pembaruan atribut lainnya sesuai kebutuhan aplikasi

            Peserta updatedPeserta = pesertaRepository.save(peserta);
            return PesertaMapper.mapToPesertaDto(updatedPeserta);
        }
        return null;
    }
    @Override
    public boolean hasExistingPeserta(Long id) {
        return pesertaRepository.existsByUserId(id);
    }
    @Override
    public void deletePeserta(Long id) {
        Optional<Peserta> pesertaOptional = pesertaRepository.findById(id);
        if (pesertaOptional.isPresent()) {
            Peserta peserta = pesertaOptional.get();
            User user = peserta.getUser();
            if (user != null) {
                // Hapus relasi dengan menghapus Peserta dari User
                user.setPeserta(null);
                userRepository.save(user);
            }
            // Hapus Peserta dari database
            pesertaRepository.delete(peserta);
        }
    }
    
    @Override
    public List<PesertaDto> getPesertaByEmail(String email) {
    User user = userRepository.findByEmail(email);
    if (user == null) {
        // Handle user not found case
        return Collections.emptyList();
    }

    List<Peserta> pesertaList = pesertaRepository.findByUser(user);
    return pesertaList.stream()
            .map(PesertaMapper::mapToPesertaDto)
            .collect(Collectors.toList());
    }
    
    //Pagination
    @Override
       public Page<Peserta> findByNamaContainingIgnoreCase
            (String keyword, Pageable pageable){
        return pesertaRepository.findByNamaContainingIgnoreCase(keyword, pageable);
    }
    @Override
    public Page<Peserta> findAll(Pageable pageable){
       return pesertaRepository.findAll(pageable);
    }
    
}
