package com.rafirizha.projectuas.service.impl;

import com.rafirizha.projectuas.dto.JenisPerlombaanDto;
import com.rafirizha.projectuas.entity.JenisPerlombaan;
import com.rafirizha.projectuas.mapper.JenisPerlombaanMapper;
import com.rafirizha.projectuas.repository.JenisPerlombaanRepository;
import com.rafirizha.projectuas.service.JenisPerlombaanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JenisPerlombaanServiceImpl implements JenisPerlombaanService {

    private final JenisPerlombaanRepository jenisPerlombaanRepository;

    @Autowired
    public JenisPerlombaanServiceImpl(JenisPerlombaanRepository jenisPerlombaanRepository) {
        this.jenisPerlombaanRepository = jenisPerlombaanRepository;
    }

    @Override
    public JenisPerlombaanDto getJenisPerlombaanById(Long id) {
        JenisPerlombaan jenisPerlombaan = jenisPerlombaanRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Jenis perlombaan not found"));
        return JenisPerlombaanMapper.mapToJenisPerlombaanDto(jenisPerlombaan);
    }

    @Override
    public List<JenisPerlombaanDto> getAllJenisPerlombaan() {
        List<JenisPerlombaan> jenisPerlombaanList = jenisPerlombaanRepository.findAll();
        return jenisPerlombaanList.stream()
                .map(JenisPerlombaanMapper::mapToJenisPerlombaanDto)
                .collect(Collectors.toList());
    }

    @Override
    public JenisPerlombaanDto createJenisPerlombaan(JenisPerlombaanDto jenisPerlombaanDto) {
        JenisPerlombaan jenisPerlombaan = JenisPerlombaanMapper.mapToJenisPerlombaan(jenisPerlombaanDto);
        JenisPerlombaan createdJenisPerlombaan = jenisPerlombaanRepository.save(jenisPerlombaan);
        return JenisPerlombaanMapper.mapToJenisPerlombaanDto(createdJenisPerlombaan);
    }

    @Override
    public JenisPerlombaanDto updateJenisPerlombaan(Long id, JenisPerlombaanDto jenisPerlombaanDto) {
        JenisPerlombaan existingJenisPerlombaan = jenisPerlombaanRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Jenis perlombaan not found"));

        existingJenisPerlombaan.setNama(jenisPerlombaanDto.getNama());
        JenisPerlombaan updatedJenisPerlombaan = jenisPerlombaanRepository.save(existingJenisPerlombaan);
        return JenisPerlombaanMapper.mapToJenisPerlombaanDto(updatedJenisPerlombaan);
    }

    @Override
    public void deleteJenisPerlombaan(Long id) {
        jenisPerlombaanRepository.deleteById(id);
    }
}
