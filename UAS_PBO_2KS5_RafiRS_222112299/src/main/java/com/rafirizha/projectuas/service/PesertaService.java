/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.rafirizha.projectuas.service;
import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.entity.Peserta;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PesertaService{
    List<PesertaDto> getAllPeserta();
    PesertaDto getPesertaById(Long id);
    PesertaDto createPeserta(PesertaDto pesertaDto);
    PesertaDto updatePeserta(Long id, PesertaDto pesertaDto);
    void deletePeserta(Long id);
//    void addPeserta(PesertaDto timDto, Long userId);
    boolean hasExistingPeserta(Long id);
    List<PesertaDto> getPesertaByEmail(String email);
    
    public Page<Peserta> findByNamaContainingIgnoreCase
            (String keyword, Pageable pageable);
    public Page<Peserta> findAll(Pageable pageable);
}

