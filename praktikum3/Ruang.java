/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

/**
 * @author 222112299_Rafi Rizha Syakhari
 * Class ruang berisikan informasi tentang nama ruang di sebuah gedung
 */
public class Ruang {
    private String namaRuang;

    public Ruang(String namaRuang){
        this.namaRuang = namaRuang;    
    }

    /**
     * @return the namaRuang
     */
    public String getNamaRuang() {
        return namaRuang;
    }

    /**
     * @param namaRuang the namaRuang to set
     */
    public void setNamaRuang(String namaRuang) {
        this.namaRuang = namaRuang;
    }
}
