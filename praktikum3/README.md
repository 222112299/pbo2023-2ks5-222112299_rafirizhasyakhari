## Praktikum3

**Rafi Rizha Syakhari**

Pada praktikum3 ini dipelajari tentang konsep dalam oop java, yakni
- Inheritance
- Polymorphism
- Aggregation
- Composition

Program pada praktikum3 menjelaskan tentang sebuah sistem suatu kantor dimana terdapat pegawai, gedung, divisi kerja dan masing-masing unsur tersebut diimplementasikan di dalam Class dalam oop java.
