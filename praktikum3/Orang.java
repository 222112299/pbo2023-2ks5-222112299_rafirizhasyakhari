/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

import java.util.Date;
/**
 * @author 222112299_Rafi Rizha Syakhari
 * Class Orang merupakan parent class dari Pegawai, berisikan atribut nama dan tanggal lahir
 * berisi constructor, methods, dan method setter and getter
 */
public class Orang {
    private String nama;
    private Date tanggalLahir;

    public Orang(){
    }

    public Orang(String nama){
        this.nama = nama;
    }

    public Orang(String nama, Date tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the tanggalLahir
     */
    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    /**
     * @param tanggalLahir the tanggalLahir to set
     */
    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getGaji(){
        return "Tidak Ada";
    }
}
