/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;
import java.util.Date;

/**
 * @author 222112299_Rafi Rizha Syakhari
 * Class Pegawai merupakan child class dari Orang, berisikan informasi atribut, constructor, serta method di dalamnya untuk Pegawai
 */
public class Pegawai extends Orang{
    private String NIP;
    private String namaKantor;
    private String unitKerja;

    public Pegawai(){
    }

    public Pegawai(String NIP, String namaKantor, String unitKerja){
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }

    public Pegawai(String nama, String NIP, String namaKantor, String unitKerja){
        super(nama);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }

    public Pegawai(String nama, Date tanggalLahir, String NIP, String namaKantor, String unitKerja){
        super(nama, tanggalLahir);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }

    /**
     * @return the NIP
     */
    public String getNIP() {
        return NIP;
    }

    /**
     * @param NIP the NIP to set
     */
    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    /**
     * @return the namaKantor
     */
    public String getNamaKantor() {
        return namaKantor;
    }

    /**
     * @param namaKantor the namaKantor to set
     */
    public void setNamaKantor(String namaKantor) {
        this.namaKantor = namaKantor;
    }

    /**
     * @return the unitKerja
     */
    public String getUnitKerja() {
        return unitKerja;
    }

    /**
     * @param unitKerja the unitKerja to set
     */
    public void setUnitKerja(String unitKerja) {
        this.unitKerja = unitKerja;
    }

    @Override
    public String getGaji(){
        return "7 Juta";
    }
}
