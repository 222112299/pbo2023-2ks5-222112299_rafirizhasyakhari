/*
* Class Java di atas adalah definisi dari sebuah entity bernama Student, yang merepresentasikan tabel dalam database yang memiliki nama
* tbl_student. Beberapa atribut dari entity tersebut memiliki mapping pada kolom-kolom dalam tabel yang bersangkutan.
*
* @Annotation Secara Umum
* @Setter, @Getter merupakan anotasi dari library Lombok yang secara otomatis men-generate method setter dan getter untuk semua atribut dalam class.
* @AllArgsConstructor merupakan anotasi dari library Lombok yang secara otomatis men-generate constructor dengan parameter untuk semua atribut dalam class.
* @NoArgsConstructor merupakan anotasi dari library Lombok yang secara otomatis men-generate constructor tanpa parameter (default constructor) untuk class
* tersebut. @Builder merupakan anotasi dari library Lombok yang secara otomatis men-generate builder pattern untuk class tersebut. @Entity merupakan anotasi
* yang menandakan bahwa class tersebut merupakan entity dalam ORM (Object Relational Mapping). @Table(name = "tbl_student") merupakan anotasi yang menandaka
* bahwa entity tersebut memetakan tabel dengan nama "tbl_student" di dalam database.

* @Annotation Dalam Field
* @Id menandakan bahwa atribut tersebut merupakan primary key dari entity. @GeneratedValue(strategy = GenerationType.IDENTITY) menandakan bahwa nilai dari primary
* key tersebut akan di-generate secara otomatis oleh database ketika entity tersebut disimpan ke dalam database. @Column(nullable = false) menandakan bahwa kolom
* dalam tabel yang bersangkutan tidak dapat memiliki nilai null. @CreationTimestamp merupakan anotasi dari library Hibernate yang secara otomatis men-generate nilai
* timestamp ketika entitas tersebut dibuat (dalam hal ini, ketika disimpan ke dalam database). @UpdateTimestamp merupakan anotasi dari library Hibernate yang secara
* otomatis men-generate nilai timestamp ketika entitas tersebut di-update (dalam hal ini, ketika disimpan ke dalam database). @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
* menandakan bahwa atribut bertipe Date harus di-format dalam format tanggal ISO (yyyy-MM-dd).
*
* class Student.java lebih berfokus untuk data pada database nya, sehingga @Annotation yang dilakukan akan lebih konfigurasi pada suatu database
* class StudentDto.java lebih berfokus pada bagaimana data ini akan digunakan di dalam View dan bagaimana bentuk validasinya di form nanti
*/

package com.rafirs.student.democrudwebapp.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ASUS
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tbl_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthDate;
    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @CreationTimestamp
    private Date createdOn;
    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn;
}
