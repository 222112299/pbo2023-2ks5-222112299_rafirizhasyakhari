/*
* Class StudentRepository merupakan sebuah interface yang meng-extend JpaRepository dari Spring Data JPA. Interface ini bertugas
* untuk melakukan operasi CRUD (Create, Read, Update, Delete) terhadap data mahasiswa (entity Student) ke database.
*
* Interface ini memiliki beberapa method abstract bawaan dari JpaRepository seperti findAll(), findById(), save(), delete(), dan deleteById().
* Selain itu, di dalam interface ini terdapat sebuah method abstract baru bernama findByLastName(String lastName) yang akan digunakan untuk
* mencari data mahasiswa berdasarkan nama belakang (lastName)-> Method abstract findByLastName(String lastName) ini akan mengembalikan sebuah
* objek Optional dari entity Student berdasarkan pencarian dari kolom "LastName"
*/
package com.rafirs.student.democrudwebapp.repository;

import com.rafirs.student.democrudwebapp.entity.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ASUS
 */
public interface StudentRepository extends JpaRepository<Student, Long>{
    Optional<Student> findByLastName (String lastName);
}
