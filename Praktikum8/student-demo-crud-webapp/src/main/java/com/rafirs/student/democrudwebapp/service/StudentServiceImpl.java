/*
 * Class Java di atas merupakan implementasi dari interface StudentService yang telah dijelaskan sebelumnya. Class ini berisi
* implementasi dari semua metode yang didefinisikan dalam interface tersebut.Class ini memiliki anotasi @Service, yang menunjukkan
* bahwa Class ini adalah sebuah layanan (service) yang akan di-inject di dalam aplikasi Spring. Kelas ini memiliki constructor dengan
* parameter StudentRepository, yang akan di-inject secara otomatis oleh Spring karena kelas ini ditandai sebagai @Service.
* 1. Metode ambilDaftarStudent() akan mengambil daftar semua data mahasiswa dari repository, kemudian mengonversi setiap objek Student menjadi StudentDto
*    menggunakan StudentMapper, dan mengembalikan daftar StudentDto.
* 2. Metode hapusDataStudent() akan menghapus data mahasiswa berdasarkan id yang diberikan, menggunakan method deleteById() pada StudentRepository.
* 3. Metode perbaruiDataStudent() akan mengambil StudentDto sebagai parameter, kemudian akan mengonversinya menjadi objek Student menggunakan StudentMapper,
*    dan akan menyimpan objek Student yang diperbarui menggunakan save() pada StudentRepository.
* 4. Metode simpanDataStudent() akan mengambil StudentDto sebagai parameter, kemudian akan mengonversinya menjadi objek Student menggunakan StudentMapper,
*    dan akan menyimpan objek Student yang baru menggunakan save() pada StudentRepository.
* 5. Metode cariById() akan mencari data mahasiswa berdasarkan id yang diberikan menggunakan method findById() pada StudentRepository, kemudian akan mengonversi
*    objek Student yang ditemukan menjadi StudentDto menggunakan StudentMapper, dan akan mengembalikan StudentDto tersebut.
 */
package com.rafirs.student.democrudwebapp.service;

import com.rafirs.student.democrudwebapp.dto.StudentDto;
import com.rafirs.student.democrudwebapp.entity.Student;
import com.rafirs.student.democrudwebapp.mapper.StudentMapper;
import com.rafirs.student.democrudwebapp.repository.StudentRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS
 */
@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
    @Override
    public List<StudentDto> ambilDaftarStudent(){
        List<Student> students = this.studentRepository.findAll();
        List<StudentDto> studentDtos = students.stream()
                .map((student)->(StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());
        return studentDtos;
    }
    
    public StudentDto ambilDaftarStudent(Long studentId){
        Optional<Student> student = this.studentRepository.findById(studentId);
        if (student.isPresent()) {
            return StudentMapper.mapToStudentDto(student.get());    
        } else {
            return null;
        }
                
    }
    @Override
    public void hapusDataStudent(Long studentId){
        this.studentRepository.deleteById(studentId);
    }

    @Override
    public void perbaruiDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }

    @Override
    public void simpanDataStudent(StudentDto studentDto){
    Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student); 
       
    }
    
    @Override
    public StudentDto cariById(Long id){
        Student student = studentRepository.findById(id).orElse(null);
        StudentDto studentDto = StudentMapper.mapToStudentDto(student);
        return studentDto;
    }
}
