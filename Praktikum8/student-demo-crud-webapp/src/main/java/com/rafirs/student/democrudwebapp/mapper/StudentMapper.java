/*
* Class StudentMapper adalah sebuah mapper class yang berfungsi untuk melakukan pemetaan data dari objek Student ke objek StudentDto dan sebaliknya.
* Dalam class ini terdapat 2 method, yaitu mapToStudentDto dan mapToStudent, yang keduanya memiliki fungsi yang berbeda.
*
* Method mapToStudentDto digunakan untuk melakukan pemetaan data dari objek Student ke objek StudentDto. Pada method ini, dilakukan pembuatan objek StudentDto
* dengan menggunakan builder pattern dari lombok, dan nilai-nilai atribut pada objek Student ditransfer ke atribut pada objek StudentDto.
*
* Method mapToStudent digunakan untuk melakukan pemetaan data dari objek StudentDto ke objek Student. Pada method ini, dilakukan pembuatan objek Student dengan
* menggunakan builder pattern dari lombok, dan nilai-nilai atribut pada objek StudentDto ditransfer ke atribut pada objek Student. Dengan adanya class ini,
* kita bisa memisahkan model data yang ada di database layer dan ada di view layer menjadi class berbeda, yang kemudian dengan class ini bisa dilakukan konversi
* antar model tersebut dengan mudah dan efisien
 */
package com.rafirs.student.democrudwebapp.mapper;

import com.rafirs.student.democrudwebapp.dto.StudentDto;
import com.rafirs.student.democrudwebapp.entity.Student;

/**
 *
 * @author ASUS
 */
public class StudentMapper {
    public static StudentDto mapToStudentDto(Student student){
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .birthDate(student.getBirthDate())
                .createdOn(student.getCreatedOn())
                .updatedOn(student.getUpdatedOn())
                .build();
        return studentDto;
    }
    
    public static Student mapToStudent(StudentDto studentDto){
        Student student = Student.builder()
                .id(studentDto.getId())
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .birthDate(studentDto.getBirthDate())
                .createdOn(studentDto.getBirthDate())
                .updatedOn(studentDto.getUpdatedOn())
                .build();
        return student;
    }

}

