/*
* Class Java di atas adalah sebuah interface yang bernama StudentService. Interface ini memiliki 5 method yang harus diimplementasikan
* oleh kelas-kelas yang mengimplementasikan interface ini. Method-method tersebut adalah:
* 1. ambilDaftarStudent(): method ini akan mengambil daftar student dalam bentuk List<StudentDto>.
* 2. perbaruiDataStudent(StudentDto studentDto): method ini akan melakukan perbaruan data student berdasarkan data studentDto yang diberikan.
* 3. hapusDataStudent(Long studentId): method ini akan menghapus data student dengan id yang sesuai dengan parameter yang diberikan.
* 4. simpanDataStudent(StudentDto studentDto): method ini akan menyimpan data student baru berdasarkan data studentDto yang diberikan.
* 5. cariById(Long id): method ini akan mencari data student dengan id yang sesuai dengan parameter yang diberikan dan mengembalikan data student
*    dalam bentuk StudentDto.
 */
package com.rafirs.student.democrudwebapp.service;
import com.rafirs.student.democrudwebapp.dto.StudentDto;
import com.rafirs.student.democrudwebapp.entity.Student;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface StudentService {
    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent(StudentDto studentDto);
    public void hapusDataStudent(Long studentId);
    public void simpanDataStudent(StudentDto studentDto);
    public StudentDto cariById(Long id);
}
