/*
* Class StudentDto memiliki beberapa atribut seperti id, firstName, lastName, birthDate, createdOn, dan updatedOn yang merepresentasikan data dari
* sebuah entitas Student. Atribut-atribut tersebut diberikan annotation seperti @NotEmpty dan @NotNull yang berfungsi sebagai validasi inputan user
* saat melakukan input data. Jadi dengan memanfaatkan @Annotation tersebut kita bisa mmebuat field beserat validasi fieldnya dengan mudah
*
* Penjelasan @Annotation
* Selain itu, terdapat annotation @Data, @Builder, @NoArgsConstructor, dan @AllArgsConstructor yang berasal dari Lombok library. Annotation @Data
* digunakan untuk mengenerate getter, setter, equals, hashCode, dan toString secara otomatis. Annotation @Builder digunakan untuk membuat objek
* StudentDto dengan menggunakan pattern Builder sehingga dapat membuat objek dengan lebih mudah dan fleksibel. Annotation @NoArgsConstructor dan
* @AllArgsConstructor digunakan untuk mengenerate constructor kosong dan constructor dengan parameter secara otomatis.Terakhir, terdapat annotation
* @DateTimeFormat yang digunakan untuk mengkonversi data tipe Date menjadi String dalam format tertentu yang diinginkan.
*
* class Student.java lebih berfokus untuk data pada database nya, sehingga @Annotation yang dilakukan akan lebih konfigurasi pada suatu database
* class StudentDto.java lebih berfokus pada bagaimana data ini akan digunakan di dalam View dan bagaimana bentuk validasinya di form nanti
*/
package com.rafirs.student.democrudwebapp.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author ASUS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    private Long id;
    @NotEmpty(message = "First Name should not be empty")
    private String firstName;
    @NotEmpty(message = "Last Name should not be empty")
    private String lastName;
    @NotNull(message = "You must input your birthdate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdOn;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn;
}

