/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.entity;

import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 *
 * @author ASUS
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tim")
@Getter
@Setter
public class Tim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String nama;

    @OneToMany(mappedBy = "tim", cascade = CascadeType.ALL)
    private List<Peserta> pesertaList;
    
    @OneToOne
    @JoinColumn(name = "jenis_perlombaan_id", referencedColumnName = "id")
    private JenisPerlombaan jenisPerlombaan;
    
    @Transient
    private String jenisPerlombaanNama;
    
    public String getJenisPerlombaanNama(){
        if (jenisPerlombaan != null){
            return jenisPerlombaan.getNama();
        }
        return jenisPerlombaanNama;
    }
    
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = true)
    private User user;

}

