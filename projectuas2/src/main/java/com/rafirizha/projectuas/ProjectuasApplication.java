package com.rafirizha.projectuas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectuasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectuasApplication.class, args);
	}

}
