package com.rafirizha.projectuas.repository;

import com.rafirizha.projectuas.entity.JenisPerlombaan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JenisPerlombaanRepository extends JpaRepository<JenisPerlombaan, Long> {
    // Tambahkan method findById
    JenisPerlombaan findByIdAndTimListId(Long id, Long timId);
}
