/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.rafirizha.projectuas.entity.Role;
/**
 *
 * @author pinaa
 */

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}

