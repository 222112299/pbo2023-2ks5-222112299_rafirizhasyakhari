package com.rafirizha.projectuas.repository;

import com.rafirizha.projectuas.entity.Tim;
import com.rafirizha.projectuas.entity.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TimRepository extends JpaRepository<Tim, Long> {
    // Tambahkan method findById
    Tim findByIdAndJenisPerlombaanId(Long id, Long jenisPerlombaanId);
    Tim findByUserId(Long id);
    public boolean existsByUserId(Long userId);
    List<Tim> findByUser(User user);
    
//    Page<Tim> findByNameContainingIgnoreCase(String keyword, Pageable pageable);
//    Pageable
//    @Query("SELECT s from Student s WHERE " +
//            " s.firstName LIKE CONCAT('%', :query, '%') OR " +
//            " s.lastName LIKE CONCAT('%', :query, '%')")
//    List<Tim> searchStudent(String query);
}
