package com.rafirizha.projectuas.repository;

import com.rafirizha.projectuas.entity.Peserta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PesertaRepository extends JpaRepository<Peserta, Long> {
    // Tambahkan method findById
    Peserta findByIdAndTimId(Long id, Long timId);
}


