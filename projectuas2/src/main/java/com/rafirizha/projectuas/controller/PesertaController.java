package com.rafirizha.projectuas.controller;

import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.service.PesertaService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;



@Controller
@RequestMapping("/peserta")
public class PesertaController {
    private final PesertaService pesertaService;

    @Autowired
    public PesertaController(PesertaService pesertaService) {
        this.pesertaService = pesertaService;
    }

    @GetMapping("/form")
    public String showForm(Model model) {
        model.addAttribute("pesertaDto", new PesertaDto());
        return "peserta-form";
    }

    @PostMapping("/create")
    public String createPeserta(@Valid @ModelAttribute("pesertaDto") PesertaDto pesertaDto, BindingResult result) {
        if (result.hasErrors()) {
            return "peserta-form";
        }
        pesertaService.createPeserta((List<PesertaDto>) pesertaDto);
        return "redirect:/peserta/list";
    }

    @GetMapping("/list")
    public String showPesertaList(Model model) {
        model.addAttribute("pesertaList", pesertaService.getAllPeserta());
        return "peserta-list";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") Long id, Model model) {
        PesertaDto pesertaDto = pesertaService.getPesertaById(id);
        model.addAttribute("pesertaDto", pesertaDto);
        return "peserta-form";
    }

    @PostMapping("/update/{id}")
    public String updatePeserta(@PathVariable("id") Long id, @Valid @ModelAttribute("pesertaDto") PesertaDto pesertaDto, BindingResult result) {
        if (result.hasErrors()) {
            return "peserta-form";
        }
        pesertaService.updatePeserta(id, pesertaDto);
        return "redirect:/peserta/list";
    }

    @GetMapping("/delete/{id}")
    public String deletePeserta(@PathVariable("id") Long id) {
        pesertaService.deletePeserta(id);
        return "redirect:/peserta/list";
    }
}

