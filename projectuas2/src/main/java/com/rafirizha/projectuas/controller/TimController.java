/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.controller;

import com.rafirizha.projectuas.dto.TimDto;
import com.rafirizha.projectuas.dto.JenisPerlombaanDto; // Tambahkan import ini
import com.rafirizha.projectuas.service.TimService;
import com.rafirizha.projectuas.service.JenisPerlombaanService; // Tambahkan import ini
import com.rafirizha.projectuas.dto.PesertaDto; // Tambahkan import ini
import com.rafirizha.projectuas.entity.User;
import com.rafirizha.projectuas.security.CustomUserDetailsService;
import com.rafirizha.projectuas.service.PesertaService;
import jakarta.validation.Valid;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.security.Principal;

import java.util.List; // Tambahkan import ini
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Controller
@RequestMapping("/tim")
public class TimController {
    private TimService timService;
    private JenisPerlombaanService jenisPerlombaanService; // Tambahkan ini
    private PesertaService pesertaService; // Tambahkan ini
    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    public TimController(TimService timService
            , JenisPerlombaanService jenisPerlombaanService
            , PesertaService pesertaService
            , CustomUserDetailsService userDetailsService){
        this.timService = timService;
        this.jenisPerlombaanService = jenisPerlombaanService;
        this.pesertaService = pesertaService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/form")
    public String showForm(Model model) {
        model.addAttribute("timDto", new TimDto());
        return "tim-form";
    }

    @PostMapping("/create")
    public String createTim(@ModelAttribute("timDto") TimDto timDto) {
        timService.createTim(timDto);
        return "redirect:/tim/list";
    }
    @GetMapping("/admin/list")
    public String showsTimList(Model model) {
        model.addAttribute("timList", timService.getAllTim());
        return "/admin/admin-tim-list";
    }
    
        @GetMapping("/teamlist")
    public String showedTimList(Model model) {
        model.addAttribute("timList", timService.getAllTim());
        return "list-of-tim";
    }
    
    @GetMapping("/list")
    public String showTimList(Model model, Authentication authentication) {
            String currentPrincipalName = authentication.getName();
            List<TimDto> timList = timService.getTimByEmail(currentPrincipalName);
            
        model.addAttribute("timList", timList);
//        model.addAttribute("timList", timService.getAllTim());
        return "tim-list";
    }

    @GetMapping("/edit/{id}")
    public String showEditForm(@PathVariable("id") Long id, Model model) {
        TimDto timDto = timService.getTimById(id);
        
        
        List<JenisPerlombaanDto> jenisPerlombaanList = jenisPerlombaanService.getAllJenisPerlombaan();
        model.addAttribute("jenisPerlombaanList", jenisPerlombaanList);
        model.addAttribute("timDto", timDto);
        return "update-form";
    }

    @PostMapping("/update/{id}")
    public String updateTim(@PathVariable("id") Long id, @ModelAttribute("timDto") TimDto timDto) {
        timService.updateTim(id, timDto);
        return "redirect:/tim/list";
    }

    @GetMapping("/delete/{id}")
    public String deleteTim(@Valid @PathVariable("id") Long id) {
        timService.deleteTim(id);
        return "redirect:/tim/list";
    }
    
    @GetMapping("/daftar-perlombaan")
    public String showDaftarPerlombaanForm(Model model) {
        TimDto timDto = new TimDto();
        List<JenisPerlombaanDto> jenisPerlombaanList = jenisPerlombaanService.getAllJenisPerlombaan();
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserId = authentication.getName();
        Long userId = userDetailsService.getIdUserByUsername(currentUserId);
        
        timDto.setUserId(userId);
        
        model.addAttribute("currentUserId", userId); // Tambahkan atribut currentUserId
        model.addAttribute("timDto", new TimDto());
        model.addAttribute("jenisPerlombaanList", jenisPerlombaanList);

        return "daftar-perlombaan-form";
    }

        @PostMapping("/daftar-perlombaan/process")
        public String processDaftarPerlombaan(
            @ModelAttribute("timDto") TimDto timDto, Authentication authentication) {
            String currentPrincipalName = authentication.getName();
            Long userId = userDetailsService.getIdUserByUsername(currentPrincipalName);
            
            timDto.setUserId(userId);
//            Long userId = userDetailsService.getIdUserByUsername(currentUserId);
//            timDto.setUserId(userId);
            
//        Long jenisPerlombaanId = timDto.getJenisPerlombaan().getId();
//        JenisPerlombaanDto jenisPerlombaan = jenisPerlombaanService.getJenisPerlombaanById(jenisPerlombaanId);
//        timDto.setJenisPerlombaan(jenisPerlombaan);
        
            timService.createTim(timDto);
      

        return "redirect:/tim/list";
    }
}


///*
// * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
// * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
// */
//package com.rafirizha.projectuas.controller;
//
//import com.rafirizha.projectuas.dto.TimDto;
//import com.rafirizha.projectuas.service.TimService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
///**
// *
// * @author ASUS
// */
//@Controller
//@RequestMapping("/tim")
//public class TimController {
//    private final TimService timService;
//
//    @Autowired
//    public TimController(TimService timService) {
//        this.timService = timService;
//    }
//
//    @GetMapping("/form")
//    public String showForm(Model model) {
//        model.addAttribute("timDto", new TimDto());
//        return "tim-form";
//    }
//
//    @PostMapping("/create")
//    public String createTim(@ModelAttribute("timDto") TimDto timDto) {
//        timService.createTim(timDto);
//        return "redirect:/tim/list";
//    }
//
//    @GetMapping("/list")
//    public String showTimList(Model model) {
//        model.addAttribute("timList", timService.getAllTim());
//        return "tim-list";
//    }
//
//    @GetMapping("/edit/{id}")
//    public String showEditForm(@PathVariable("id") Long id, Model model) {
//        TimDto timDto = timService.getTimById(id);
//        model.addAttribute("timDto", timDto);
//        return "tim-form";
//    }
//
//    @PostMapping("/update/{id}")
//    public String updateTim(@PathVariable("id") Long id, @ModelAttribute("timDto") TimDto timDto) {
//        timService.updateTim(id, timDto);
//        return "redirect:/tim/list";
//    }
//
//    @GetMapping("/delete/{id}")
//    public String deleteTim(@PathVariable("id") Long id) {
//        timService.deleteTim(id);
//        return "redirect:/tim/list";
//    }
//}
