package com.rafirizha.projectuas.mapper;

import com.rafirizha.projectuas.dto.JenisPerlombaanDto;
import com.rafirizha.projectuas.entity.JenisPerlombaan;

public class JenisPerlombaanMapper {
    public static JenisPerlombaanDto mapToJenisPerlombaanDto(JenisPerlombaan jenisPerlombaan) {
        JenisPerlombaanDto jenisPerlombaanDto = new JenisPerlombaanDto();
        jenisPerlombaanDto.setId(jenisPerlombaan.getId());
        jenisPerlombaanDto.setNama(jenisPerlombaan.getNama());
        // Tidak memanggil TimMapper di sini untuk menghindari rekursi tak terbatas
        return jenisPerlombaanDto;
    }

    public static JenisPerlombaan mapToJenisPerlombaan(JenisPerlombaanDto jenisPerlombaanDto) {
        JenisPerlombaan jenisPerlombaan = new JenisPerlombaan();
        jenisPerlombaan.setId(jenisPerlombaanDto.getId());
        jenisPerlombaan.setNama(jenisPerlombaanDto.getNama());
        // Tidak memanggil TimMapper di sini untuk menghindari rekursi tak terbatas
        return jenisPerlombaan;
    }
}
