/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.mapper;

import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.entity.Peserta;

public class PesertaMapper {
    public static PesertaDto mapToPesertaDto(Peserta peserta) {
        // Membuat dto dengan builder pattern (inject dari lombok)
        PesertaDto pesertaDto = PesertaDto.builder()
                .id(peserta.getId())
                .nama(peserta.getNama())
                .tim(TimMapper.mapToTimDto(peserta.getTim()))
                .build();
        return pesertaDto;
    }

    public static Peserta mapToPeserta(PesertaDto pesertaDto) {
        Peserta peserta = Peserta.builder()
        .id(pesertaDto.getId())
        .nama(pesertaDto.getNama())
        .tim(TimMapper.mapToTim(pesertaDto.getTim()))
        .build();
        return peserta;
    }
}

