package com.rafirizha.projectuas.mapper;

import com.rafirizha.projectuas.dto.TimDto;
import com.rafirizha.projectuas.entity.Tim;

public class TimMapper {
    public static TimDto mapToTimDto(Tim tim) {
        TimDto timDto = new TimDto();
        timDto.setId(tim.getId());
        timDto.setNama(tim.getNama());
        timDto.setJenisPerlombaan(JenisPerlombaanMapper.mapToJenisPerlombaanDto(tim.getJenisPerlombaan()));
        timDto.setJenisPerlombaanNama(tim.getJenisPerlombaanNama());
//        timDto.setUser(UserMapper.mapToUserDto(tim.getUser()));
        return timDto;
    }

    public static Tim mapToTim(TimDto timDto) {
        Tim tim = new Tim();
        tim.setId(timDto.getId());
        tim.setNama(timDto.getNama());
        tim.setJenisPerlombaan(JenisPerlombaanMapper.mapToJenisPerlombaan(timDto.getJenisPerlombaan()));
        tim.setUser(UserMapper.mapToUser(timDto.getUser()));
        return tim;
    }
}
