package com.rafirizha.projectuas.service;

import com.rafirizha.projectuas.dto.PesertaDto;

import java.util.List;

public interface PesertaService {

    List<PesertaDto> getAllPeserta();

    PesertaDto getPesertaById(Long id);

    List<PesertaDto> createPeserta(List<PesertaDto> pesertaDtoList);

    PesertaDto updatePeserta(Long id, PesertaDto pesertaDto);

    void deletePeserta(Long id);
}
