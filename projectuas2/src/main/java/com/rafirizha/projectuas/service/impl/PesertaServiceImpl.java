package com.rafirizha.projectuas.service.impl;

import com.rafirizha.projectuas.dto.PesertaDto;
import com.rafirizha.projectuas.entity.Peserta;
import com.rafirizha.projectuas.mapper.PesertaMapper;
import com.rafirizha.projectuas.repository.PesertaRepository;
import com.rafirizha.projectuas.service.PesertaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PesertaServiceImpl implements PesertaService {

    private final PesertaRepository pesertaRepository;

    public PesertaServiceImpl(PesertaRepository pesertaRepository) {
        this.pesertaRepository = pesertaRepository;
    }

    @Override
    public List<PesertaDto> getAllPeserta() {
        List<Peserta> pesertaList = pesertaRepository.findAll();
        return pesertaList.stream()
                .map(PesertaMapper::mapToPesertaDto)
                .collect(Collectors.toList());
    }

    @Override
    public PesertaDto getPesertaById(Long id) {
        Optional<Peserta> pesertaOptional = pesertaRepository.findById(id);
        return pesertaOptional.map(PesertaMapper::mapToPesertaDto).orElse(null);
    }

    @Override
    public List<PesertaDto> createPeserta(List<PesertaDto> pesertaDtoList) {
        List<Peserta> pesertaList = pesertaDtoList.stream()
                .map(PesertaMapper::mapToPeserta)
                .collect(Collectors.toList());
        List<Peserta> savedPesertaList = pesertaRepository.saveAll(pesertaList);
        return savedPesertaList.stream()
                .map(PesertaMapper::mapToPesertaDto)
                .collect(Collectors.toList());
    }

    @Override
    public PesertaDto updatePeserta(Long id, PesertaDto pesertaDto) {
        Optional<Peserta> pesertaOptional = pesertaRepository.findById(id);
        if (pesertaOptional.isPresent()) {
            Peserta peserta = pesertaOptional.get();
            peserta.setNama(pesertaDto.getNama());
            // Melakukan pembaruan atribut lainnya sesuai kebutuhan aplikasi

            Peserta updatedPeserta = pesertaRepository.save(peserta);
            return PesertaMapper.mapToPesertaDto(updatedPeserta);
        }
        return null;
    }

    @Override
    public void deletePeserta(Long id) {
        pesertaRepository.deleteById(id);
    }
}
