package com.rafirizha.projectuas.service.impl;

import com.rafirizha.projectuas.dto.TimDto;
import com.rafirizha.projectuas.entity.JenisPerlombaan;
import com.rafirizha.projectuas.entity.Tim;
import com.rafirizha.projectuas.entity.User;
import com.rafirizha.projectuas.mapper.JenisPerlombaanMapper;
import com.rafirizha.projectuas.mapper.TimMapper;
import com.rafirizha.projectuas.repository.TimRepository;
import com.rafirizha.projectuas.repository.UserRepository;
import com.rafirizha.projectuas.service.TimService;
import java.util.Collections;
import java.util.HashSet;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class TimServiceImpl implements TimService {

    private final TimRepository timRepository;
    private final UserRepository userRepository;

    @Autowired
    public TimServiceImpl(TimRepository timRepository, UserRepository userRepository) {
        this.timRepository = timRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<TimDto> getAllTim() {
        List<Tim> timList = timRepository.findAll();
        return timList.stream()
                .map(TimMapper::mapToTimDto)
                .collect(Collectors.toList());
    }

    @Override
    public TimDto getTimById(Long id) {
        Optional<Tim> timOptional = timRepository.findById(id);
        return timOptional.map(TimMapper::mapToTimDto).orElse(null);
    }

    @Override
    public TimDto createTim(TimDto timDto) {
        Long userId = timDto.getUserId();
        Tim existingTim = timRepository.findByUserId(userId);
        
        if (existingTim != null) {
        throw new IllegalStateException("Tim already exists for the specified user.");
    }
        Tim tim = TimMapper.mapToTim(timDto);
        Tim savedTim = timRepository.save(tim);
        return TimMapper.mapToTimDto(savedTim);
    }

    @Override
    public TimDto updateTim(Long id, TimDto timDto) {
        Optional<Tim> timOptional = timRepository.findById(id);
        if (timOptional.isPresent()) {
            Tim tim = timOptional.get();
            tim.setNama(timDto.getNama());
            JenisPerlombaan jenisPerlombaan = JenisPerlombaanMapper.mapToJenisPerlombaan(timDto.getJenisPerlombaan());
            tim.setJenisPerlombaan(jenisPerlombaan);
            

            // Lakukan pembaruan atribut lainnya sesuai kebutuhan aplikasi

            Tim updatedTim = timRepository.save(tim);
            return TimMapper.mapToTimDto(updatedTim);
        }
        return null;
    }
    @Override
    public boolean hasExistingTim(Long id) {
        return timRepository.existsByUserId(id);
    }
    @Override
    public void deleteTim(Long id) {
        Optional<Tim> timOptional = timRepository.findById(id);
        if (timOptional.isPresent()) {
            Tim tim = timOptional.get();
            User user = tim.getUser();
            if (user != null) {
                // Hapus relasi dengan menghapus Tim dari User
                user.setTim(null);
                userRepository.save(user);
            }
            // Hapus Tim dari database
            timRepository.delete(tim);
        }
    }
    
    @Override
    public List<TimDto> getTimByEmail(String email) {
    User user = userRepository.findByEmail(email);
    if (user == null) {
        // Handle user not found case
        return Collections.emptyList();
    }

    List<Tim> timList = timRepository.findByUser(user);
    return timList.stream()
            .map(TimMapper::mapToTimDto)
            .collect(Collectors.toList());
    }

//    @Override
//    public void deleteTim(Long id) {
//        timRepository.deleteById(id);
//    }
    
    @Override
    public void addTim(TimDto timDto, Long userId){
        User user = UserServiceImpl.unwrapUser(userRepository.findById(userId),userId);
        Tim tim = TimMapper.mapToTim(timDto);
        tim.setUser(user);
        timRepository.save(tim);
    }
    
}
