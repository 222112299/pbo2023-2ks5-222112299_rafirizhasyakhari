/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.rafirizha.projectuas.service;
import com.rafirizha.projectuas.dto.TimDto;
import com.rafirizha.projectuas.entity.Tim;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TimService{
    List<TimDto> getAllTim();
    TimDto getTimById(Long id);
    TimDto createTim(TimDto timDto);
    TimDto updateTim(Long id, TimDto timDto);
    void deleteTim(Long id);
    void addTim(TimDto timDto, Long userId);
    boolean hasExistingTim(Long id);
    List<TimDto> getTimByEmail(String email);
    
    // Pagination
//    public Page<Tim> findByNameContainingIgnoreCase
//            (String keyword, Pageable pageable);
//    public Page<Tim> findAll(Pageable pageable);
}

