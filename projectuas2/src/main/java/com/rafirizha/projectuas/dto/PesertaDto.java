package com.rafirizha.projectuas.dto;

//import jakarta.persistence.*;
//import jakarta.validation.constraints.Email;
//import jakarta.validation.constraints.NotNull;
//import jakarta.validation.constraints.Size;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.rafirizha.projectuas.entity.Peserta;
import java.util.List;

/**
 *
 * @author ASUS
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PesertaDto {
    private Long id;
    
    @NotBlank(message = "Nama peserta harus diisi")
    private String nama;
    
    private TimDto tim;
    
    private List<PesertaDto> pesertaDtoList;
    
    public void addPeserta(PesertaDto peserta){
        this.pesertaDtoList.add(peserta);
    }
}