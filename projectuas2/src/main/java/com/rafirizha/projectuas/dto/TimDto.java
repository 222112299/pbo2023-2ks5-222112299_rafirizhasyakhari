/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rafirizha.projectuas.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
/**
 *
 * @author ASUS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TimDto {
    private Long id;

    @NotBlank(message = "Nama tim harus diisi")
    private String nama;
    
    private JenisPerlombaanDto jenisPerlombaan;
    
    private String jenisPerlombaanNama;
    
    private List<PesertaDto> pesertaDtoList;
    
    private UserDto user;
    
    public void addPeserta(PesertaDto peserta){
        this.pesertaDtoList.add(peserta);
    }
    
    public void setUserId(Long userId){
        this.user = new UserDto();
        this.user.setId(userId);
    }
    
    public Long getUserId(){
        return user.getId();
    }
    

    
    
//    public void setJenisPerlombaanId(Long jenisPerlombaanId){
//        this.jenisPerlombaan = new JenisPerlombaanDto();
//        this.jenisPerlombaan.setId(jenisPerlombaanId);
//    }
   
    public void setJenisPerlombaanNama(String jenisPerlombaanNama) {
        this.jenisPerlombaanNama = jenisPerlombaanNama;
        }
}
//    @NotNull(message = "Daftar ID peserta tidak boleh kosong")
//    private List<Long> pesertaIdList;
//
//    @NotNull(message = "ID jenis perlombaan tidak boleh kosong")
//    private Long jenisPerlombaanId;

    // Constructor, getter, setter, dan lainnya
