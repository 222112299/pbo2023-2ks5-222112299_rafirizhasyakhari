/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 * @author 222112299_Rafi Rizha Syakhari
 * Class Main dari Mahasiswa3 Class
 */
public class MahasiswaMain3 {
        public static void main(String[] args) {
        Mahasiswa3 s1 = new Mahasiswa3(); //Menginstansiasi objek Mahasiswa3
        Mahasiswa3 s2 = new Mahasiswa3(); //Menginstansiasi objek Mahasiswa3
        //Inisialisasi variabel
        s1.nim = 123456;
        s1.nama = "Luth";
        s2.nim = 1234578;
        s2.nama = "Rahma";
        System.out.println(s1.nim+" "+s1.nama);
        System.out.println(s2.nim+" "+s2.nama);    
    }
}
