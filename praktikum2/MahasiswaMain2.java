/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 * @author 222112299_Rafi Rizha Syakhari
 * Class Main dari Mahasiswa2 Class
 */
public class MahasiswaMain2 {


    public static void main(String[] args) {
        Mahasiswa2 s1 = new Mahasiswa2(); //Menginstansiasi objek Mahasiswa2
        s1.nim = 12345;     //Inisialisasi variabel
        s1.nama = "Luthfi"; //Inisialisasi variabel
        System.out.println(s1.nim);
        System.out.println(s1.nama);
    }  
    
}
