/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 * @author 222112299_Rafi Rizha Syakhari
 * Class Main dari Mahasiswa3 Class
 */
public class MahasiswaMain5 {
    public static void main(String[] args) {
        Mahasiswa4 s1 = new Mahasiswa4(123456, "Luthfi");   //Menginstansiasi objek Mahasiswa4
        Mahasiswa4 s2 = new Mahasiswa4(123457, "Rahma");    //Menginstansiasi objek Mahasiswa4
        
        s1.tampilkanInfo();
        s2.tampilkanInfo();
    }
}
